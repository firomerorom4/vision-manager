angular.module('myday.services',[])
    .factory('EntityManager', function ($q,$timeout) {
        var em = {};
        em.LIST = 'LIST';
        em.ADD = 'ADD';
        em.EDIT = 'EDIT';
        em.DELETE = 'DELETE';
        em.FIND = 'FIND';
        em.COUNT = 'COUNT';
        em.PAGE = 'PAGE';
        em.NORMAL = 'NORMAL';
        em.VIRTUAL = '360';


        //dbs es la base de datos interna solo con los datos
        //panorama es la base de datos con las imagenes 360, los panoramas.



        /**
         * CRUD Vision
         * @param vision
         * @param action
         */
        em.vision = function(vision,action)
        {
            var sql;
            var st;
            var d=$q.defer();
            var result = [];
            switch (action)
            {
                case em.LIST:
                    sql = "select * from vision;";
                    try{
                        st = dbs.prepare(sql);
                        while (st.step()){
                            result.push( st.getAsObject());
                        }

                        st.free();
                        d.resolve(result);
                    }
                    catch (err){
                        d.reject(err);
                    }

                    break;
                case em.PAGE:
                    sql = "select * from vision limit "+vision.limit+" offset "+vision.offset+";";
                   try{
                       st = dbs.prepare(sql);
                       while (st.step()){

                           result.push(st.getAsObject());
                       }

                       st.free();
                       d.resolve(result);
                   }
                    catch (err){
                        d.reject(err);
                    }

                    break;
                case em.ADD:
                    sql= "insert into vision (titulo,contenido,pie) values (?,?,?);";
                    try{
                        dbs.run(sql,[vision.titulo,vision.contenido,vision.pie]);
                        em.writeData().then(function(){
                            d.resolve(true);
                        });

                    }
                    catch (err){
                        d.reject(err);
                    }
                    break;
                case em.EDIT:
                    sql = "update vision set titulo = '"+vision.titulo+ "', contenido='"+vision.contenido+"', pie ='"+vision.pie+"' where id="+vision.id+";";
                    try{
                        dbs.run(sql);
                        em.writeData().then(function(){
                            d.resolve(true);
                        });

                    }
                    catch (err){
                        d.reject(err);
                    }
                    break;
                case em.DELETE:
                    sql = "delete from vision where id="+vision.id+";";
                    try{
                        dbs.run(sql);
                        em.writeData().then(function(){
                            d.resolve(true);
                        });

                    }
                    catch (err){
                        d.reject(err);
                    }
                    break;
                case em.COUNT:
                    sql = "select count(*) as 'count' from vision;";
                    try{
                        st = dbs.prepare(sql);
                        while (st.step()){
                            result.push( st.getAsObject());
                        }

                        st.free();
                        d.resolve(result[0].count);
                    }
                    catch (err){
                        d.reject(err);
                    }

                    break;
                default:
                    d.reject({message:'Sin operacion'});
                    break;

            }



            return d.promise;

        };



        /**
         *
         * @param query
         * @returns {*}
         */
        em.results = function (query) {

            var d=$q.defer();
            var result = [];
            var st;
            try{
                st = dbs.prepare(query);
                while (st.step()){

                    result.push(st.getAsObject());
                }

                st.free();
                d.resolve(result);
            }
            catch (err){
                d.reject(err);
            }

            return d.promise;

        };


        /**
         *
         * @param query
         * @returns {*}
         */
        em.runs = function(query){
            var d=$q.defer();
            try{
                dbs.run(query);
                d.resolve(true);
            }
            catch (err){
                d.reject(err);
            }

            return d.promise;
        };

        /**
         * write regular database
         */
        em.writeData = function ()
        {
            var d = $q.defer();
            try{

                $timeout(function () {
                    var data = dbs.export();
                    var buffer = new Buffer(data);
                    fs.writeFileSync("vision.db", buffer);
                    d.resolve(buffer);
                },2000);

            }
            catch (reason){
                d.reject(reason);

            }

            return d.promise;

        };

        return em;
    })

;

