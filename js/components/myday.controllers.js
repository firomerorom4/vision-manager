angular.module('myday.controllers',[])
    .controller('VisionController',VisionController)
    .controller('SideMenuController',SideMenuController)
    .controller('ModalController',ModalController)
;
/**
 * Psalm Controller
 * @param $scope

 * @param $uibModal
 * @param EntityManager


 * @constructor
 */
function VisionController($scope,$uibModal,EntityManager) {
$scope.records = [];
$scope.total = 5;
$scope.pagination = {
    current: 1
};
var limit = 5;

    /**
     *
     * @param record
     * @param size
     * @constructor
     */
    $scope.Record= function (record,size)
    {
        var instance = $uibModal.open({
            animation: true,
            templateUrl: "js/templates/modal.html",
            controller: 'ModalController',
            size: size,
            resolve:
            {
                record:function ()
                {
                    return record;
                }

            }
        });

        instance.result.then(function() {
            List(1);
        }).catch(function(reason){
            console.log(reason)
        });
    };

    $scope.Delete = function(record, size) {
        var instance = $uibModal.open({
            animation: true,
            templateUrl: "js/templates/delete-modal.html",
            controller: function ($scope,record,$uibModalInstance) {
                $scope.record = record;
                $scope.Close = function () {
                    Close();
                };

                $scope.Delete = function (record) {
                    Finish(record.tipo);
                };

                function Close() {
                    $uibModalInstance.dismiss('cancel');
                }

                function Finish(data) {
                    $uibModalInstance.close(data==undefined?'close':data);
                }
            },
            size: size,
            resolve: {
                record: function () {
                    return record;
                }
            }
        });

        instance.result.then(function(data) {
            return Delete(record).then(function () {
                List(1);
            }) ;
        });
    };

    $scope.isActive = function (r) {
      return r=='vision';
    };

    $scope.pageChanged = function(newPage) {
        List(newPage);
    };


    function Delete (record){
       return EntityManager.vision(record,EntityManager.DELETE);
    }

    function List(offset){
        if (offset==undefined)
        {
            EntityManager.vision({},EntityManager.LIST).then(function(result){
                $scope.records = result;
                $scope.total = result.length;
            }).catch(function(reason){
                console.log(reason)
            });
        }
        else
        {
            EntityManager.vision({offset:(offset-1)*limit,limit:limit},EntityManager.PAGE).then(function(result){
                $scope.records = result;
                $scope.total = result.length;
            }).catch(function(reason){
                console.log(reason)
            });
        }

        EntityManager.vision({},EntityManager.COUNT).then(function(amount){
            if ($scope.total!=amount) {
                $scope.total=amount;
            }
        });
}

    List();

}

/**
 * SideMenu
 * @param $scope
 * @constructor
 */
function SideMenuController($scope){

    $scope.active ='about';

    $scope.setAtive = function (ref) {
        $scope.active = ref;
    };

    $scope.ExportDatabase = function ()
    {
       try{
           var data = dbs.export();
           var data2 = panorama.export();
           var buffer = new Buffer(data);
           var buffer2 = new Buffer(data2);
           fs.writeFileSync("gocuba.db", buffer);
           fs.writeFileSync("panorama.db", buffer2);
           alert('Base de datos guardada');
       }
        catch (err){
            alert('Error');
        }
    }
}

/**
 *
 * @param $uibModalInstance
 * @param $scope
 * @param Manager
 * @param EntityManager
 * @param Upload
 * @param record
 * @constructor
 */
function ModalController($uibModalInstance,$scope,record,EntityManager) {
    $scope.alert =
    {
        show:false,
        message:'',
        type:'success'
    };

    $scope.record = record;

    /**
     * UI Close
     *
     */
    $scope.Close = function () {
        Close();
    };

    /**
     * UI save
     * @param record
     *
     */
    $scope.Save=function(record)
    {

        try{

            if (record.id_vision!=undefined)
            {

                EntityManager.vision(record,EntityManager.EDIT).then(function () {
                    Finish('ok');
                }).catch(function (reason) {
                    Close(reason);
                });
            }
            else
            {

                EntityManager.vision(record,EntityManager.ADD).then(function () {
                    Finish('ok');
                }).catch(function (reason) {
                    Close(reason);
                });
            }




        }
        catch (err){
            Close(err);
        }

    };

    /**
     * UI Delete
     * @param record
     *
     */
    $scope.Delete = function(record){
      Manager.delete(record).then(function(data){
          Finish(ntype);
      }).catch(function(reason){
          Close(reason);
      });
    };




    /**
     * On cancel
     * @param reason
     *
     */
    function Close(reason) {
        $uibModalInstance.dismiss(reason==undefined?'cancel':reason);
    }

    /**
     * Finish Operation
     * @param data     *
     */
    function Finish(data) {
        $uibModalInstance.close(data==undefined?'close':data);
    }
}