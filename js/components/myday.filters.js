angular.module('myday.filters',[])
.filter('sp',function () {
    return function (data)
    {
        if(data.indexOf('á')!=-1)
        {
            data = data.replace(data,'/á/','\u00e1');
        }
        if(data.indexOf('é')!=-1)
        {
            data = data.replace(data,'/é/','\u00e9');

        }
        if(data.indexOf('í')!=-1)
        {
            data = data.replace(data,'/í/','\u00ed');

        }
        if(data.indexOf('ó')!=-1)
        {
            data = data.replace(data,'/ó/','\u00f3');

        }
        if(data.indexOf('ú')!=-1)
        {
            data = data.replace(data,'/ú/','\u00fa');

        }

        if(data.indexOf('ñ')!=-1)
        {
            data = data.replace(data,'/ñ/','\u00f1');

        }
        if(data.indexOf('Á')!=-1)
        {
            data = data.replace(data,'/Á/','\u00c1');

        }


        return data;

    }
})

;