'use strict';
var fs = require('fs');
var os = require('os');
var ws = {};
var sql = require('sql.js/js/sql-memory-growth.js');
var bfr = fs.readFileSync('./vision.db');
var dbs = new sql.Database(bfr);
fs.exists(os.tmpdir()+'/.sao/data',function (exists) {
    if (!exists) {
        fs.mkdir(os.tmpdir()+'/.sao');
        fs.mkdir(os.tmpdir()+'/.sao/data');
    }
    ws = fs.createWriteStream(os.tmpdir()+'/.sao/data/sao.json');
});

var MemoryStream = require('memorystream');
angular.module('myday',['ngAnimate','ui.router','ui.scroll','myday.controllers','myday.services','myday.filters','ui.bootstrap','ngSanitize','angularUtils.directives.dirPagination'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/about');

        $stateProvider
            .state('vision',{
            url:'/vision',
            templateUrl:'js/templates/vision.html',
            controller: "VisionController"
                     })
           .state('about',{
                url:'/about',
                templateUrl:'js/templates/about.html'

            })
    })


;
